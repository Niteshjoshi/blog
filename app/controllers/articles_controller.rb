class ArticlesController < ApplicationController
 http_basic_authenticate_with name: "nitesh", password: "secret", except: [:index, :show]
	
  def new
		@article = Article.new
	end
	
	def edit
    @article = Article.find(params[:id])
	end
    
  def index
    @articles = Article.all
  end

  def view_all
    @articles=Article.all
    render :action => :index
    return
  end	
	
	def show
    @article = Article.find(params[:id])
  end
 
	def create
		# render plain: params[:article].inspect
		@article = Article.new(article_params)
		 # debugger
    if @article.save
       redirect_to article_path(@article.id)
    else
       render 'new'
    end
  end

  def update
  @article = Article.find(params[:id])
 
  if @article.update(article_params)
    redirect_to article_path(@article.id)
  else
    render 'edit'
  end
end

  def destroy
      @article=Article.find(params[:id])
      @article.destroy
      redirect_to :action => 'index'
  end
  
  private
  def article_params
    params.require(:article).permit(:title, :text)
  end
	
end
# Article.new({:title => 'val','text' => 'val',})